let i = Number(prompt("Enter a number: "));

for(i; i>=0; i-=5){

    if(i <=50){
        console.log("The current value is at 50. Terminating the loop.");
        break;
    }
    if (i % 10 ===0){
        console.log("The number is divisible by 10. Skipping the number.");
        continue;
    }
    
    if(((i%5)===0) && ((i%10)!==0))
    {
        console.log(i);
    }
}

let longWord = "supercalifragilisticexpialidocious";
let consonant ="";

for(let i=0; i<longWord.length;i++){
    if (longWord[i].toLowerCase()==='a'||longWord[i].toLowerCase()==='e'||longWord[i].toLowerCase()==='i'||longWord[i].toLowerCase()==='o'||longWord[i].toLowerCase()==='u'){
        continue;
    }
    else{
        consonant += longWord[i];
    }

}
console.log(longWord);
console.log(consonant);